"use strict";

$(document).ready(function() {
    class View {
        constructor () {
            this.menu = $(".menu");
            this.menuBtns = $(".menu a");
            this.deviceMenuBtn = $("#device-menu-btn");
            this.moreBtn = $(".more");
            this.seoText = $("#seo-text");
            this.hiddenSeoText = $("#hidden-seo-text");
            this.seoTextLink = $(".seo-text-link");
        }
    };

    class Model {
        constructor () {
            this.moreBtnText = ["More", "Less"];
        }
    };

    class Controller {
        constructor (view, model) {
            this.view = view;
            this.model = model;
            this.slideIndex = 0;
        }

        bindEvents() {
            this.view.moreBtn.click(() => {
                event.preventDefault();
                if (this.view.moreBtn.html() === this.model.moreBtnText[0]) {
                    this.view.seoText.stop();
                    this.view.moreBtn.html(this.model.moreBtnText[1]);
                    this.view.hiddenSeoText.slideDown();
                } else {
                    this.view.seoText.stop();
                    this.view.moreBtn.html(this.model.moreBtnText[0]);
                    this.view.hiddenSeoText.slideUp();
                }
            });

            this.view.menuBtns.each((index, element) => {
                $(element).click(() => {
                    this.removeActive();
                    $(element).addClass('active');
                })
            });

            this.view.deviceMenuBtn.click(() => {
                this.view.menu.stop();
                this.view.menu.slideToggle(2000);
            });
        }

        removeActive() {
            this.view.menuBtns.each((index, element) => {
                $(element).removeClass('active');
            });
        }

        init() {
            this.bindEvents();
        }

    };

    var appView = new View();
    var appModel = new Model();
    var appController = new Controller(appView, appModel);
    appController.init();
});